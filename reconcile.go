package main

import (
	"fmt"
	"strconv"
)

func reconcileState() {
	fmt.Printf("Starting Slurm <-> Kubernetes reconciliation loop\n")
	jobs, err := jobStatus()
	if err != nil {
		fmt.Println(err)
		return
	}

	podList, err := getPods()
	if err != nil {
		fmt.Println(err)
		return
	}

	// Find jobs with no matching pods and remove them
	// Also remove pods when job is no longer running
	for _, job := range jobs.Jobs {
		var located = false

		if job.Name != "k8s-bridge" {
			continue
		}

		for _, pod := range podList.Items {
			jobint, _ := strconv.Atoi(pod.Metadata.Annotations["slurm.net/job_id"])
			if jobint != job.JobId {
				continue
			}

			located = true

			fmt.Printf("JobId=%d State=%s SlurmNodes=%s K8sNodes=%s\n", job.JobId, job.State, job.Nodes, pod.Spec.NodeName)
			switch job.State {
			case "PENDING":
				fmt.Printf("JobId=%d is still pending\n", job.JobId)
			case "RUNNING":
				// ensure label has been applied
				if pod.Spec.NodeName == "" {
					fmt.Printf("JobId=%d has started running, binding now\n", job.JobId)
					knode, err := nodelookup(job.Nodes)
					if err != nil {
						fmt.Printf("JobId=%d Nodes=%d cannot locate K8s nodes\n", job.JobId, job.Nodes)
						break
					}
					bind(&pod, knode)
				}
			case "CANCELLED", "COMPLETED":
				fmt.Printf("JobId=%d has finished, stopping pod\n", job.JobId)
				deletePod(&pod)
			}

			break
		}

		if !located {
			switch job.State {
			case "RUNNING", "PENDING":
				// no corresponding pod left, kill the Slurm job to release resources
				fmt.Printf("JobId=%d State=%s has no corresponding pod, cancelling\n", job.JobId, job.State)
				cancelJob(job.JobId)
			}
		}
	}

	// Find pods with no matching jobs and remove them
	for _, pod := range podList.Items {
		var located = false

		// this should have been filtered previously, but do this as a sanity check anyways
		if pod.Spec.Scheduler != schedulerName {
			fmt.Printf("%s %s\n", pod.Metadata.Name, pod.Spec.Scheduler)
			continue
		}

		if pod.Metadata.Annotations["slurm.net/job_id"] == "" {
			// fixme - submit corresponding job and update label
			fmt.Printf("Pod %s has no corresponding JobId\n", pod.Metadata.Name)
			continue
		}

		jobint, _ := strconv.Atoi(pod.Metadata.Annotations["slurm.net/job_id"])

		for _, job := range jobs.Jobs {
			if jobint == job.JobId {
				located = true
				break
			}
		}

		if !located {
			fmt.Printf("Pod %s corresponding JobId=%d not found, deleting\n", pod.Metadata.Name, jobint)
			deletePod(&pod)
		}
	}
}
