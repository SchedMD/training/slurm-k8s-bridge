package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os/exec"
)

type JobList struct {
	Jobs []Job
}

type Job struct {
	Comment string `json:"comment"`
	JobId   int    `json:"job_id"`
	Name    string `json:"name"`
	Nodes   string `json:"nodes"`
	State   string `json:"job_state"`
}

func jobStatus() (JobList, error) {
	var jobs JobList

	resp, err := exec.Command("squeue", "--json").Output()
	if err != nil {
		fmt.Println(err)
		return JobList{}, err
	}

	err = json.NewDecoder(bytes.NewReader(resp)).Decode(&jobs)
	if err != nil {
		fmt.Println(err)
		return JobList{}, err
	}

	return jobs, nil
}
