package main

import (
	"fmt"
	"os/exec"
	"strconv"
)

func cancelJob(jobId int) {
	// restrict cancellation to jobs named "k8s-bridge" as a safety measure
	res, err := exec.Command("scancel", "-n", "k8s-bridge", strconv.Itoa(jobId)).Output()

	if err != nil {
		fmt.Println(res)
		fmt.Println(err)
	}
}
