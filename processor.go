// Copyright 2016 Google Inc. All Rights Reserved.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

var processorLock = &sync.Mutex{}

func reconcileUnscheduledPods(interval int, done chan struct{}, wg *sync.WaitGroup) {
	for {
		select {
		case <-time.After(time.Duration(interval) * time.Second):
			err := schedulePods()
			if err != nil {
				log.Println(err)
			}
		case <-done:
			wg.Done()
			log.Println("Stopped reconciliation loop.")
			return
		}
	}
}

func monitorUnscheduledPods(done chan struct{}, wg *sync.WaitGroup) {
	pods, errc := watchUnscheduledPods()

	for {
		select {
		case err := <-errc:
			log.Println(err)
		case pod := <-pods:
			var podCreated = false
			processorLock.Lock()
			err := submitPod(&pod, &podCreated)
			if err != nil {
				log.Println(err)
			}
			if podCreated {
				time.Sleep(time.Second)
			}
			reconcileState()
			processorLock.Unlock()
		case <-done:
			wg.Done()
			log.Println("Stopped scheduler.")
			return
		}
	}
}

func submitPod(pod *Pod, jobsCreated *bool) error {
	var err error

	jobid := pod.Metadata.Annotations["slurm.net/job_id"]
	if jobid == "" {
		fmt.Println("Submitting new job corresponding to new pod", pod.Metadata.Name)
		jobid, err = submit(pod)
		if err != nil {
			return err
		}
		pod.Metadata.Annotations = map[string]string{
			"slurm.net/job_id": jobid,
		}
		*jobsCreated = true
	} else {
		fmt.Printf("Already have JobId=%s for pod %s\n", jobid, pod.Metadata.Name)
	}

	return nil
}

func schedulePods() error {
	var jobsCreated = false
	processorLock.Lock()
	defer processorLock.Unlock()
	pods, err := getUnscheduledPods()
	if err != nil {
		log.Println(err)
		return err
	}

	for _, pod := range pods {
		err := submitPod(pod, &jobsCreated)
		if err != nil {
			log.Println(err)
		}
	}

	// Give the slurmctld a chance to place any newly submitted jobs
	if jobsCreated {
		fmt.Println("Stalling for newly submitted jobs to potentially start")
		time.Sleep(time.Second)
	}

	reconcileState()

	return nil
}
