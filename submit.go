package main

import (
	"errors"
	"fmt"
	"os/exec"
)

func submit(pod *Pod) (string, error) {
	result, err := exec.Command("sbatch", "--parsable", "--wrap", "sleep 100000", "-o", "/dev/null", "-e", "/dev/null", "-J", "k8s-bridge").Output()
	if err != nil {
		fmt.Println(err)
		return "", errors.New("sbatch failed")
	}

	if len(result) == 0 {
		return "", errors.New("sbatch output empty?!")
	}

	// strip trailing newline
	jobid := string(result[0 : len(result)-1])
	fmt.Printf("Pod %s mapped to JobId=%s\n", pod.Metadata.Name, jobid)

	err = label(pod, jobid)
	// on error we should probably cancel the slurm side, otherwise this will never reconcile

	return jobid, err
}
