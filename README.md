# Slurm Kubernetes Scheduling Plugin (slurm-k8s-bridge)

## Compiling

```
go build
```

### Running

The system you run the bridge from must have access to the Slurm user commands
(`sbatch`, `scancel`, `squeue`). Slurm jobs corresponding to each pod will be
submitted and accounted for under that user account.

```
kubectl proxy
./slurm-k8s-bridge
```
